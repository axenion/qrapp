package lt.codeAcademy.qrApp.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lt.codeAcademy.qrApp.QrAppApplication;
import lt.codeAcademy.qrApp.openapi.model.BeneficiaryInfoDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = QrAppApplication.class)
@AutoConfigureMockMvc
@WithMockUser(username = "some_admin", authorities = {"ROLE_USER", "ROLE_ADMIN"})
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:data.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:clean.sql")
})
@ActiveProfiles("test")
public class BeneficiaryInfoApiITest {
    @Autowired
    private MockMvc restMockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testGet() throws Exception {
        MvcResult result = restMockMvc.perform(get("/beneficiary-info/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        BeneficiaryInfoDTO dto = objectMapper.readValue(result.getResponse().getContentAsByteArray(), BeneficiaryInfoDTO.class);
        Assertions.assertEquals(1, dto.getId());
        Assertions.assertEquals("Edwin", dto.getName());
        Assertions.assertEquals("fjds6432d", dto.getUniqueCode());
    }

    @Test
    public void testGetNotFound() throws Exception {
        restMockMvc.perform(get("/beneficiary-info/10")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void testPost() throws Exception {
        BeneficiaryInfoDTO benDto = new BeneficiaryInfoDTO();
        benDto.setName("Jackie");
        benDto.setUniqueCode("15phgibuj4");

        MvcResult result = restMockMvc.perform(post("/beneficiary-info")
                        .content(objectMapper.writeValueAsBytes(benDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        BeneficiaryInfoDTO dto = objectMapper.readValue(result.getResponse().getContentAsByteArray(), BeneficiaryInfoDTO.class);
        Assertions.assertNotNull(dto.getId());
        Assertions.assertEquals("Jackie", dto.getName());
        Assertions.assertEquals("15phgibuj4", dto.getUniqueCode());
    }

    @Test
    public void testPostWithId() throws Exception {

        BeneficiaryInfoDTO benDto = new BeneficiaryInfoDTO();
        benDto.setId(21L);
        benDto.setName("Jackie");
        benDto.setUniqueCode("15phgibuj4");

        restMockMvc.perform(post("/beneficiary-info")
                        .content(objectMapper.writeValueAsBytes(benDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testPostWithDuplicateUniqueCode() throws Exception {
        BeneficiaryInfoDTO benDto = new BeneficiaryInfoDTO();
        benDto.setName("Jackie");
        benDto.setUniqueCode("fjds6432d3");

        restMockMvc.perform(post("/beneficiary-info")
                        .content(objectMapper.writeValueAsBytes(benDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDelete() throws Exception {
        restMockMvc.perform(delete("/beneficiary-info/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testDeleteNotFound() throws Exception {
        restMockMvc.perform(delete("/beneficiary-info/10")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testPut() throws Exception {
        BeneficiaryInfoDTO benDto = new BeneficiaryInfoDTO();
        benDto.setId(4L);
        benDto.setName("Jackie");
        benDto.setUniqueCode("897wqsd2");

        MvcResult result = restMockMvc.perform(put("/beneficiary-info")
                        .content(objectMapper.writeValueAsBytes(benDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        BeneficiaryInfoDTO dto = objectMapper.readValue(result.getResponse().getContentAsByteArray(), BeneficiaryInfoDTO.class);
        Assertions.assertEquals(4, dto.getId());
        Assertions.assertEquals("Jackie", dto.getName());
        Assertions.assertEquals("897wqsd2", dto.getUniqueCode());
    }

    @Test
    public void testPutWithoutId() throws Exception {
        BeneficiaryInfoDTO benDto = new BeneficiaryInfoDTO();
        benDto.setName("Jackie");
        benDto.setUniqueCode("15phgibuj4");

        restMockMvc.perform(put("/beneficiary-info")
                        .content(objectMapper.writeValueAsBytes(benDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testPutWithDuplicateUniqueCodes() throws Exception {
        BeneficiaryInfoDTO benDto = new BeneficiaryInfoDTO();
        benDto.setId(3L);
        benDto.setName("Jackie");
        benDto.setUniqueCode("fjds6432d3");

        restMockMvc.perform(put("/beneficiary-info")
                        .content(objectMapper.writeValueAsBytes(benDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
