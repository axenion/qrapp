package lt.codeAcademy.qrApp.api;

import lt.codeAcademy.qrApp.QrAppApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = QrAppApplication.class)
@AutoConfigureMockMvc
@WithMockUser(username = "some_admin", authorities = {"ROLE_USER", "ROLE_ADMIN"})
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:data.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:clean.sql")
})
@ActiveProfiles("test")
public class QrApiITest {
    @Autowired
    private MockMvc restMockMvc;

    @Test
    public void testGet() throws Exception {
        restMockMvc.perform(get("/qr")
                        .param("uniqueCode", "fjds6432d")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetNotFound() throws Exception {
        restMockMvc.perform(get("/qr")
                        .param("uniqueCode", "4fg37676wve")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


}
