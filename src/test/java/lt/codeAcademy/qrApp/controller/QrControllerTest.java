package lt.codeAcademy.qrApp.controller;

import lt.codeAcademy.qrApp.api.QrController;
import lt.codeAcademy.qrApp.service.QRCodeService;
import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import lt.codeAcademy.qrApp.service.exception.QrGenerationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

@ExtendWith(MockitoExtension.class)
public class QrControllerTest {
    @Mock
    private QRCodeService qrCodeService;

    @Test
    public void getQRTestIfUniqueCodeIsNull() {
        QrController qrController = new QrController(qrCodeService);

        Assertions.assertThrows(ResponseStatusException.class, () -> qrController.getQR(null));
    }

    @Test
    public void getQRTestIfUniqueCodeIsEmpty() {
        QrController qrController = new QrController(qrCodeService);

        Assertions.assertThrows(ResponseStatusException.class, () -> qrController.getQR(""));
    }

    @Test
    public void getQRTestIfInternalServerError() throws QrAppServiceException, QrGenerationException {
        QrController qrController = new QrController(qrCodeService);
        Mockito.when(qrCodeService.generateQR(Mockito.any())).thenThrow(QrGenerationException.class);

        Assertions.assertThrows(ResponseStatusException.class, () -> qrController.getQR("1i2k34"));
    }


}
