package lt.codeAcademy.qrApp.service;

import lt.codeAcademy.qrApp.openapi.model.BeneficiaryInfoDTO;
import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class DTOValidatorTest {

    @Test
    public void testIfNull() {
        Assertions.assertThrows(QrAppServiceException.class,
                () -> DtoValidator.validate(null));
    }

    @Test
    public void testIfDtoObjectIsNull() {
        BeneficiaryInfoDTO beneficiaryInfoDTO = new BeneficiaryInfoDTO();
        Assertions.assertThrows(QrAppServiceException.class,
                () -> DtoValidator.validate(beneficiaryInfoDTO));
    }

    @Test
    public void testIfDtoNameIsNull() {
        BeneficiaryInfoDTO beneficiaryInfoDTO = new BeneficiaryInfoDTO();
        beneficiaryInfoDTO.setUniqueCode("56cnbv");
        beneficiaryInfoDTO.setId(1L);
        Assertions.assertThrows(QrAppServiceException.class,
                () -> DtoValidator.validate(beneficiaryInfoDTO));
    }

    @Test
    public void testIfDtoUniqueCodeIsNull() {
        BeneficiaryInfoDTO beneficiaryInfoDTO = new BeneficiaryInfoDTO();
        beneficiaryInfoDTO.setId(1L);
        beneficiaryInfoDTO.setName("Jackie Chan");
        Assertions.assertThrows(QrAppServiceException.class,
                () -> DtoValidator.validate(beneficiaryInfoDTO));
    }

    @Test
    public void testIfDtoIdIsNull() {
        BeneficiaryInfoDTO beneficiaryInfoDTO = new BeneficiaryInfoDTO();
        beneficiaryInfoDTO.setUniqueCode("129u03je");
        beneficiaryInfoDTO.setName("Jackie Chan");
        beneficiaryInfoDTO.setId(null);
        Assertions.assertDoesNotThrow(() -> DtoValidator.validate(beneficiaryInfoDTO));
    }

    @Test
    public void testIfDTOIsNotNull() {
        BeneficiaryInfoDTO beneficiaryInfoDTO = new BeneficiaryInfoDTO();
        beneficiaryInfoDTO.setUniqueCode("129u03je");
        beneficiaryInfoDTO.setName("Jackie Chan");
        beneficiaryInfoDTO.setId(1L);
        Assertions.assertDoesNotThrow(() -> DtoValidator.validate(beneficiaryInfoDTO));
    }
}
