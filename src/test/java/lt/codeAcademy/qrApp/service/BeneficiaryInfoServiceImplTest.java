package lt.codeAcademy.qrApp.service;

import lt.codeAcademy.qrApp.openapi.model.BeneficiaryInfoDTO;
import lt.codeAcademy.qrApp.persistence.BeneficiaryInfoRepository;
import lt.codeAcademy.qrApp.persistence.entities.BeneficiaryInfo;
import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class BeneficiaryInfoServiceImplTest {
    @Mock
    private BeneficiaryInfoRepository beneficiaryInfoRepository;

    @Test
    public void testCreate() throws QrAppServiceException {
        BeneficiaryInfo entity = new BeneficiaryInfo();
        entity.setId(2L);
        entity.setName("EdBoy");
        entity.setUniqueCode("21212122");

        Mockito.when(beneficiaryInfoRepository.existsByUniqueCode(Mockito.any())).thenReturn(false);
        Mockito.when(beneficiaryInfoRepository.save(Mockito.any())).thenReturn(entity);

        BeneficiaryInfoDTO entityDTO = new BeneficiaryInfoDTO();
        entityDTO.setName(entity.getName());
        entityDTO.setUniqueCode(entity.getUniqueCode());

        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);
        BeneficiaryInfoDTO entityStuff = serviceIMPL.create(entityDTO);

        Assertions.assertNotNull(entityStuff.getId());
        Assertions.assertEquals("EdBoy", entityStuff.getName());
        Assertions.assertEquals("21212122", entityStuff.getUniqueCode());
    }

    @Test
    public void testCreateUniqueCodeExists() throws Exception {
        Mockito.when(beneficiaryInfoRepository.existsByUniqueCode(Mockito.any())).thenReturn(true);

        BeneficiaryInfoDTO entityDTO = new BeneficiaryInfoDTO();
        entityDTO.setName("EdBoy");
        entityDTO.setUniqueCode("EdBoy221");

        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);

        Assertions.assertThrows(QrAppServiceException.class, () -> serviceIMPL.create(entityDTO));
    }

    @Test
    public void testCreateIfHasId() throws Exception {
        BeneficiaryInfoDTO entityDTO = new BeneficiaryInfoDTO();
        entityDTO.setId(2L);
        entityDTO.setName("EdBoy");
        entityDTO.setUniqueCode("EdBoy221");

        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);
        Assertions.assertThrows(QrAppServiceException.class, () -> serviceIMPL.create(entityDTO));
    }

    @Test
    public void testGet() throws QrAppServiceException {
        BeneficiaryInfo beneficiaryInfo = new BeneficiaryInfo();
        beneficiaryInfo.setUniqueCode("godzilla");
        beneficiaryInfo.setId(2L);
        beneficiaryInfo.setName("Chocolate");
        Mockito.when(beneficiaryInfoRepository.findById(beneficiaryInfo.getId())).thenReturn(Optional.of(beneficiaryInfo));

        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);
        BeneficiaryInfoDTO entity = serviceIMPL.get(beneficiaryInfo.getId());

        Assertions.assertEquals(beneficiaryInfo.getId(), entity.getId());
        Assertions.assertEquals(beneficiaryInfo.getUniqueCode(), entity.getUniqueCode());
        Assertions.assertEquals(beneficiaryInfo.getName(), beneficiaryInfo.getName());
    }

    @Test
    public void testGetIfIdDoesNotExist() {
        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);
        Assertions.assertThrows(QrAppServiceException.class, () -> serviceIMPL.get(2));
    }

    @Test
    public void testDelete() {
        Mockito.when(beneficiaryInfoRepository.existsById(2L)).thenReturn(true);
        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);

        Assertions.assertDoesNotThrow(() -> serviceIMPL.delete(2L));
    }

    @Test
    public void testDeleteIfIdDoesNotExist() {
        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);

        Assertions.assertThrows(QrAppServiceException.class, () -> serviceIMPL.delete(2L));
    }

    @Test
    public void testGetId() throws QrAppServiceException {
        BeneficiaryInfo beneficiaryInfo = new BeneficiaryInfo();
        beneficiaryInfo.setUniqueCode("godzilla");
        beneficiaryInfo.setId(2L);
        beneficiaryInfo.setName("Chocolate");
        Mockito.when(beneficiaryInfoRepository.findByUniqueCode(beneficiaryInfo.getUniqueCode())).thenReturn(beneficiaryInfo);
        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);
        long id = serviceIMPL.getId(beneficiaryInfo.getUniqueCode());

        Assertions.assertEquals(beneficiaryInfo.getId(), id);
    }

    @Test
    public void testIfUniqueCodeDoesNotExist() {
        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);

        Assertions.assertThrows(QrAppServiceException.class, () -> serviceIMPL.getId("13212452252525"));
    }

    @Test
    public void testUpdate() throws QrAppServiceException {
        BeneficiaryInfo entity = new BeneficiaryInfo();
        entity.setId(2L);

        entity.setName("EdBoy");
        entity.setUniqueCode("21212122");

        Mockito.when(beneficiaryInfoRepository.existsById(entity.getId())).thenReturn(true);

        Mockito.when(beneficiaryInfoRepository.save(Mockito.argThat(arg -> entity.getId().equals(arg.getId())))).thenReturn(entity);

        BeneficiaryInfoDTO entityDTO = new BeneficiaryInfoDTO();
        entityDTO.setId(entity.getId());
        entityDTO.setName(entity.getName());
        entityDTO.setUniqueCode(entity.getUniqueCode());

        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);
        BeneficiaryInfoDTO entityStuff = serviceIMPL.update(entityDTO);

        Assertions.assertEquals(2, entityStuff.getId());
        Assertions.assertEquals("EdBoy", entityStuff.getName());
        Assertions.assertEquals("21212122", entityStuff.getUniqueCode());
    }

    @Test
    public void updateIfUniqueCodeAlreadyExistsTest() throws QrAppServiceException {

        BeneficiaryInfo edwin = new BeneficiaryInfo();
        edwin.setId(1L);
        edwin.setName("Edwin");
        edwin.setUniqueCode("123");
        BeneficiaryInfo tom = new BeneficiaryInfo();
        tom.setId(2L);
        tom.setName("Tom");
        tom.setUniqueCode("567");

        Mockito.when(beneficiaryInfoRepository.existsById(tom.getId())).thenReturn(true);
        Mockito.when(beneficiaryInfoRepository.findByUniqueCode(edwin.getUniqueCode())).thenReturn(edwin);

        BeneficiaryInfoDTO tomUpdateDTO = new BeneficiaryInfoDTO();
        tomUpdateDTO.setId(tom.getId());
        tomUpdateDTO.setName(tom.getName());
        tomUpdateDTO.setUniqueCode(edwin.getUniqueCode());

        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);

        Assertions.assertThrows(QrAppServiceException.class, () -> serviceIMPL.update(tomUpdateDTO));
    }

    @Test
    public void updateNameTestSuccessful() throws QrAppServiceException {
        BeneficiaryInfo edwin = new BeneficiaryInfo();
        edwin.setId(1L);
        edwin.setName("Edwin");
        edwin.setUniqueCode("123");

        BeneficiaryInfo tom = new BeneficiaryInfo();
        tom.setId(2L);
        tom.setName("Tom");
        tom.setUniqueCode("567");

        BeneficiaryInfo tomUpdated = new BeneficiaryInfo();
        tomUpdated.setId(2L);
        tomUpdated.setName("Steve");
        tomUpdated.setUniqueCode("567");


        Mockito.when(beneficiaryInfoRepository.existsById(tom.getId())).thenReturn(true);
        Mockito.when(beneficiaryInfoRepository.findByUniqueCode(tom.getUniqueCode())).thenReturn(tom);
        Mockito.when(beneficiaryInfoRepository.save(Mockito.argThat(arg -> tom.getId().equals(arg.getId())))).thenReturn(tomUpdated);

        BeneficiaryInfoDTO tomUpdateDTO = new BeneficiaryInfoDTO();
        tomUpdateDTO.setId(tom.getId());
        tomUpdateDTO.setName("Steve");
        tomUpdateDTO.setUniqueCode(tom.getUniqueCode());

        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);

        Assertions.assertEquals("Steve", serviceIMPL.update(tomUpdateDTO).getName());
    }

    @Test
    public void updateIfThereIsNoId() {
        BeneficiaryInfoDTO tomUpdateDTO = new BeneficiaryInfoDTO();
        tomUpdateDTO.setId(null);
        tomUpdateDTO.setName("Steve");
        tomUpdateDTO.setUniqueCode("bghuio253");

        BeneficiaryInfoService serviceIMPL = new BeneficiaryInfoService(beneficiaryInfoRepository);

        Assertions.assertThrows(QrAppServiceException.class, () -> serviceIMPL.update(tomUpdateDTO));
    }

}
