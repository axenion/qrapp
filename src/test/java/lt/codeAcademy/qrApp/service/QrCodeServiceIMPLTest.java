package lt.codeAcademy.qrApp.service;

import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import lt.codeAcademy.qrApp.service.exception.QrGenerationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class QrCodeServiceIMPLTest {

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private BeneficiaryInfoService beneficiaryInfoService;


    @Test
    public void generateQRTestSuccessful() throws QrAppServiceException, QrGenerationException {
        Resource resource = new ClassPathResource("response.png");
        Mockito.when(restTemplate.getForEntity(Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.ok(resource));
        Mockito.when(beneficiaryInfoService.getId(Mockito.any())).thenReturn(1L);
        QRCodeService qrCodeServiceIMPL = new QRCodeService(restTemplate, beneficiaryInfoService);
        Resource resource1 = qrCodeServiceIMPL.generateQR("oijhju9pionm");
        Assertions.assertEquals(resource, resource1);
    }

    @Test
    public void generateQRTestIfServiceFails() throws QrAppServiceException {
        Mockito.when(beneficiaryInfoService.getId(Mockito.any())).thenReturn(1L);
        Mockito.when(restTemplate.getForEntity(Mockito.any(), Mockito.any())).thenReturn(ResponseEntity.badRequest().build());

        QRCodeService qrCodeServiceIMPL = new QRCodeService(restTemplate, beneficiaryInfoService);
        Assertions.assertThrows(QrGenerationException.class, () -> qrCodeServiceIMPL.generateQR("12541"));
    }

    @Test
    public void generateQRTestIfUniqueCodeDoesNotExist() throws QrAppServiceException {
        Mockito.when(beneficiaryInfoService.getId(Mockito.any())).thenThrow(QrAppServiceException.class);
        QRCodeService qrCodeServiceIMPL = new QRCodeService(restTemplate, beneficiaryInfoService);
        Assertions.assertThrows(QrAppServiceException.class, () -> qrCodeServiceIMPL.generateQR("1215125"));
    }


}

