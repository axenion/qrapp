package lt.codeAcademy.qrApp.api;

import lt.codeAcademy.qrApp.openapi.api.QrApi;
import lt.codeAcademy.qrApp.service.QRCodeService;
import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import lt.codeAcademy.qrApp.service.exception.QrGenerationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
public class QrController implements QrApi {

    private final QRCodeService qrCodeService;

    @Autowired
    public QrController(QRCodeService qrCodeService) {
        this.qrCodeService = qrCodeService;
    }

    @Override
    public ResponseEntity<Resource> getQR(@NotNull @Valid String uniqueCode) {
        if (uniqueCode == null || uniqueCode.isBlank()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No unique code was supplied");
        }
        try {
            return ResponseEntity.ok(qrCodeService.generateQR(uniqueCode));
        } catch (QrAppServiceException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (QrGenerationException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
