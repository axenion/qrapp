package lt.codeAcademy.qrApp.api;


import lt.codeAcademy.qrApp.openapi.api.BeneficiaryInfoApi;
import lt.codeAcademy.qrApp.openapi.model.BeneficiaryInfoDTO;
import lt.codeAcademy.qrApp.service.BeneficiaryInfoService;
import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class BeneficiaryInfoController implements BeneficiaryInfoApi {

    private final BeneficiaryInfoService beneficiaryInfoService;

    @Autowired
    public BeneficiaryInfoController(BeneficiaryInfoService beneficiaryInfoService) {
        this.beneficiaryInfoService = beneficiaryInfoService;
    }

    @ResponseBody
    @Override
    public ResponseEntity<BeneficiaryInfoDTO> createBeneficiary(BeneficiaryInfoDTO beneficiaryInfoDTO) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(beneficiaryInfoService.create(beneficiaryInfoDTO));
        } catch (QrAppServiceException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<BeneficiaryInfoDTO> getBeneficiary(Long id) {
        if (id == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " No id was supplied");
        }
        try {
            return ResponseEntity.ok(beneficiaryInfoService.get(id));
        } catch (QrAppServiceException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Void> deleteBeneficiary(Long id) {
        if (id == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, " No id was supplied");
        }
        try {
            beneficiaryInfoService.delete(id);
        } catch (QrAppServiceException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<BeneficiaryInfoDTO> update(BeneficiaryInfoDTO beneficiaryInfoDTO) {
        try {
            return ResponseEntity.ok(beneficiaryInfoService.update(beneficiaryInfoDTO));
        } catch (QrAppServiceException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
