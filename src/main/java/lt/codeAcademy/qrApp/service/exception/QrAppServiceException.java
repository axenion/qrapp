package lt.codeAcademy.qrApp.service.exception;

public class QrAppServiceException extends Exception {
    public QrAppServiceException(String message) {
        super(message);
    }
}
