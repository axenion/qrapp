package lt.codeAcademy.qrApp.service.exception;

public class QrGenerationException extends Exception {
    public QrGenerationException(String message) {
        super(message);
    }
}
