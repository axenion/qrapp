package lt.codeAcademy.qrApp.service;

import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import lt.codeAcademy.qrApp.service.exception.QrGenerationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class QRCodeService {
    private final RestTemplate restTemplate;
    private final BeneficiaryInfoService beneficiaryInfoService;
    @Value("${server.scheme}")
    private String scheme;
    @Value("${server.hostname}")
    private String hostName;
    @Value("${server.port}")
    private String port;

    @Autowired
    public QRCodeService(RestTemplate restTemplate, BeneficiaryInfoService beneficiaryInfoService) {
        this.restTemplate = restTemplate;
        this.beneficiaryInfoService = beneficiaryInfoService;
    }

    public Resource generateQR(String uniqueCode) throws QrGenerationException, QrAppServiceException {
        Long id = beneficiaryInfoService.getId(uniqueCode);

        ResponseEntity<Resource> response;
        try {
            response = restTemplate.getForEntity(UriComponentsBuilder.newInstance()
                    .scheme("https")
                    .host("qrcode.tec-it.com")
                    .path("/API/QRCode")
                    .queryParam("data", scheme + "://" + hostName + ":" + port + "/beneficiary-info/" + id)
                    .build().toUri(), Resource.class);
        } catch (RestClientException e) {
            throw new QrGenerationException("The QR generation service is unreachable");
        }

        if (response.getStatusCode().isError()) {
            throw new QrGenerationException("The QR generation service failed");
        }

        return response.getBody();
    }
}
