package lt.codeAcademy.qrApp.service.mappers;

import lt.codeAcademy.qrApp.openapi.model.BeneficiaryInfoDTO;
import lt.codeAcademy.qrApp.persistence.entities.BeneficiaryInfo;
import lt.codeAcademy.qrApp.persistence.entities.EntityFactory;

public class BeneficiaryInfoMapper {


    public static BeneficiaryInfo map(BeneficiaryInfoDTO dto) {
        return EntityFactory.getBeneficiaryInfo(dto.getId(),
                dto.getName(),
                dto.getUniqueCode());
    }

    public static BeneficiaryInfoDTO map(BeneficiaryInfo beneficiaryInfo) {
        return EntityFactory.getBeneficiaryInfoDTO(beneficiaryInfo.getId(),
                beneficiaryInfo.getName(),
                beneficiaryInfo.getUniqueCode());
    }
}
