package lt.codeAcademy.qrApp.service;

import lt.codeAcademy.qrApp.openapi.model.BeneficiaryInfoDTO;
import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import org.springframework.util.StringUtils;

public class DtoValidator {
    public static void validate(BeneficiaryInfoDTO beneficiaryInfoDTO) throws QrAppServiceException {

        if (beneficiaryInfoDTO == null) {
            throw new QrAppServiceException("must provide beneficiary info");
        }
        if (!StringUtils.hasText(beneficiaryInfoDTO.getName())) {
            throw new QrAppServiceException("The beneficiary name cannot be empty");
        }
        if (!StringUtils.hasText(beneficiaryInfoDTO.getUniqueCode())) {
            throw new QrAppServiceException("the unicode must not be empty");
        }
    }
}
