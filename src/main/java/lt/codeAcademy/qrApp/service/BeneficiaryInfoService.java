package lt.codeAcademy.qrApp.service;

import lt.codeAcademy.qrApp.openapi.model.BeneficiaryInfoDTO;
import lt.codeAcademy.qrApp.persistence.BeneficiaryInfoRepository;
import lt.codeAcademy.qrApp.persistence.entities.BeneficiaryInfo;
import lt.codeAcademy.qrApp.service.exception.QrAppServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static lt.codeAcademy.qrApp.service.mappers.BeneficiaryInfoMapper.map;

@Service
public class BeneficiaryInfoService {

    private final BeneficiaryInfoRepository beneficiaryInfoRepository;

    @Autowired
    public BeneficiaryInfoService(BeneficiaryInfoRepository beneficiaryInfoRepository) {
        this.beneficiaryInfoRepository = beneficiaryInfoRepository;
    }


    public BeneficiaryInfoDTO create(BeneficiaryInfoDTO beneficiaryInfoDTO) throws QrAppServiceException {
        DtoValidator.validate(beneficiaryInfoDTO);
        if (beneficiaryInfoDTO.getId() != null) {
            throw new QrAppServiceException("ID must not be set");
        }

        BeneficiaryInfo beneficiaryInfo = map(beneficiaryInfoDTO);
        if (beneficiaryInfoRepository.existsByUniqueCode(beneficiaryInfo.getUniqueCode())) {
            throw new QrAppServiceException("User with this unique code already exists");
        }
        BeneficiaryInfo saved = beneficiaryInfoRepository.save(beneficiaryInfo);

        return map(saved);
    }

    public BeneficiaryInfoDTO get(long id) throws QrAppServiceException {

        Optional<BeneficiaryInfo> k = beneficiaryInfoRepository.findById(id);
        if (k.isEmpty()) {
            throw new QrAppServiceException("User with this id does not exist");
        }

        return map(k.get());
    }

    public BeneficiaryInfoDTO update(BeneficiaryInfoDTO beneficiaryInfoDTO) throws QrAppServiceException {

        DtoValidator.validate(beneficiaryInfoDTO);
        if (beneficiaryInfoDTO.getId() == null) {
            throw new QrAppServiceException("The beneficiary Id cannot be empty");
        }
        BeneficiaryInfo beneficiaryInfo = map(beneficiaryInfoDTO);
        if (!beneficiaryInfoRepository.existsById(beneficiaryInfo.getId())) {
            throw new QrAppServiceException("There is no user with this unique code");
        }

        BeneficiaryInfo duplicateUniqueCode = beneficiaryInfoRepository.findByUniqueCode(beneficiaryInfoDTO.getUniqueCode());
        if (duplicateUniqueCode != null && !duplicateUniqueCode.getId().equals(beneficiaryInfoDTO.getId())) {
            throw new QrAppServiceException("A user with this Unique Code already exists");
        }


        BeneficiaryInfo updated = beneficiaryInfoRepository.save(beneficiaryInfo);

        return map(updated);
    }

    public void delete(Long id) throws QrAppServiceException {
        if (!beneficiaryInfoRepository.existsById(id)) {
            throw new QrAppServiceException("There is no user with this Id");
        }
        beneficiaryInfoRepository.deleteById(id);
    }

    public Long getId(String uniqueCode) throws QrAppServiceException {
        BeneficiaryInfo beneficiaryInfo = beneficiaryInfoRepository.findByUniqueCode(uniqueCode);
        if (beneficiaryInfo == null) {
            throw new QrAppServiceException("This unique code does not exist");
        }
        return beneficiaryInfo.getId();
    }

}
