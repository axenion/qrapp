package lt.codeAcademy.qrApp.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "beneficiary_info")
@Entity
public class BeneficiaryInfo extends CustomEntity<Long> {
    String name;
    String uniqueCode;

    public static BeneficiaryInfoBuilder newBuilder() {
        return new BeneficiaryInfoBuilder();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public static class BeneficiaryInfoBuilder {
        private Long id;
        private String name;
        private String uniqueCode;

        public BeneficiaryInfoBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public BeneficiaryInfoBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public BeneficiaryInfoBuilder withUniqueCode(String uniqueCode) {
            this.uniqueCode = uniqueCode;
            return this;
        }

        public BeneficiaryInfo build() {
            BeneficiaryInfo obj = new BeneficiaryInfo();
            obj.setId(id);
            obj.setName(name);
            obj.setUniqueCode(uniqueCode);
            return obj;
        }

    }

}
