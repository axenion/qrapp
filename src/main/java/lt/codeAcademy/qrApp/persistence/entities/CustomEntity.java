package lt.codeAcademy.qrApp.persistence.entities;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class CustomEntity<T extends Serializable> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    protected T id;

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
