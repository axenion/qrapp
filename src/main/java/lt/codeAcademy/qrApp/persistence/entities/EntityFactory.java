package lt.codeAcademy.qrApp.persistence.entities;

import lt.codeAcademy.qrApp.openapi.model.BeneficiaryInfoDTO;

public class EntityFactory {

    public static BeneficiaryInfo getBeneficiaryInfo(Long id, String name, String uniqueCode) {
        return BeneficiaryInfo.newBuilder()
                .withId(id)
                .withName(name)
                .withUniqueCode(uniqueCode)
                .build();
    }

    public static BeneficiaryInfoDTO getBeneficiaryInfoDTO(Long id, String name, String uniqueCode) {
        return new BeneficiaryInfoDTO()
                .id(id)
                .name(name)
                .uniqueCode(uniqueCode);
    }

}
