package lt.codeAcademy.qrApp.persistence;

import lt.codeAcademy.qrApp.persistence.entities.BeneficiaryInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeneficiaryInfoRepository extends JpaRepository<BeneficiaryInfo, Long> {

    BeneficiaryInfo findByUniqueCode(String uniqueCode);

    boolean existsByUniqueCode(String uniqueCode);
}

